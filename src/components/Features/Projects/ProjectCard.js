import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Badge, Button, Card, Col, Stack } from "react-bootstrap";
import { faPencilSquare, faTrash } from '@fortawesome/free-solid-svg-icons';
import { useDispatch } from "react-redux";
import { addProject, deleteProject, editProject, startTimer, stopTimer } from "./projectSlice";

export const ProjectCard = ({name, description, status, price, key, id}) => {
  const dispatch = useDispatch();

  const handleEditProject = () => {
    dispatch(editProject());
  }

  const handleDeleteProject = () => {
    dispatch(deleteProject());
  }

  const handleStartTimer = () => {
    dispatch(startTimer());
  } 

  const handleStopTimer = () => {
    dispatch(stopTimer());
  } 

  return (
    <Col md={3}>
      <Card border="info">
        <Card.Header>
          <Stack direction="horizontal">
            <h4 className="me-auto">{name}</h4>
            <div className="vr" />
            <p className="my-auto ms-2">{price}</p>
          </Stack>
        </Card.Header>
        <Card.Body>
          <Card.Text>
            <p>{description}</p>
            <h2 className="text-center">23:45:12:05</h2>
          </Card.Text>

          <Stack direction="horizontal" gap={3} className="mt-4">
            <Badge pill bg="info" size="md">
              {status}
            </Badge>
            <Button variant="outline-danger" onClick={handleDeleteProject} className="ms-auto" size="sm">
              <FontAwesomeIcon icon={faTrash} />
            </Button>
            <Button variant="outline-warning" onClick={handleEditProject} size="sm">
              <FontAwesomeIcon icon={faPencilSquare} />
            </Button>
          </Stack>
          <div className="d-grid mt-2">
            <Button variant="outline-success" onClick={handleStartTimer} size="sm">
              Start Timer
            </Button>
          </div>
        </Card.Body>
      </Card>
    </Col>
  );
};
