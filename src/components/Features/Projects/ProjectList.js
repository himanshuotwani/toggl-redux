import React from 'react'
import { Row } from 'react-bootstrap'
import { ProjectCard } from './ProjectCard'
import { useSelector } from 'react-redux'

export const ProjectList = () => {
    const projects = useSelector(state => state.project.projects);
    console.log(projects);
  return (
    <Row>
        {projects && projects.map( project => (
            <ProjectCard
                name={project.name}
                description={project.description}
                status={project.status}
                price={project.price}
                key={project.id}
                id={project.id}
            />
        ))}
    </Row>
  )
}
