import { createSlice } from "@reduxjs/toolkit";

const description = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam molestiae corrupti aliquam officia dolor asperiores!";
  const projects = [
    {name: "Project 1", price: "45", description, status: "Ongoing"},
    {name: "Project 2", price: "65", description, status: "Ongoing"},
    {name: "Project 2", price: "65", description, status: "Completed"}
  ];

const projectSlice = createSlice({
    name: "Projects",
    initialState: {
        projects: projects,
    },
    reducers: {
        addProject: function(currentState){
            console.log("In add");
            return { ...currentState };
        },
        editProject: function(currentState){
            console.log("In edit");
            return { ...currentState };
        },
        deleteProject: function(currentState){
            console.log("In delete");
            return { ...currentState };
        },
        startTimer: function(currentState){
            console.log("In start");
            return { ...currentState };
        },
        stopTimer: function(currentState){
            console.log("In stop");
            return { ...currentState };
        }
    }
});

export const { addProject, editProject, deleteProject, startTimer, stopTimer } = projectSlice.actions;
const projectReducer = projectSlice.reducer;
export default projectReducer;