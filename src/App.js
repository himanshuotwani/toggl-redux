import './App.css';
import { Container } from 'react-bootstrap';
import Header from './components/Layouts/Header';
import { ProjectList } from './components/Features/Projects/ProjectList';
function App() {

  return (
    <>
      <Header/>
      <Container className="mt-3">
        <ProjectList/>
      </Container>
    </>
  );
}

export default App;