import { combineReducers } from "@reduxjs/toolkit";
import projectReducer from "../../components/Features/Projects/projectSlice";

const rootReducer = combineReducers({
    project: projectReducer
});

export default rootReducer;